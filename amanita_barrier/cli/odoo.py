import click
from nameko.standalone.rpc import ServiceRpcProxy
from nameko.exceptions import RpcTimeout
import sys
from .config import load_config


@click.group(help='Odoo related commands.')
@click.pass_context
def odoo(ctx):
    pass


def write_stdout(s):
    sys.stdout.write(s)
    sys.stdout.flush()


def write_stderr(s):
    sys.stderr.write(s)
    sys.stderr.flush()


def _sync_odoo(config):
    with ServiceRpcProxy('odoo', config,
                         timeout=config.get('ODOO_SYNC_TIMEOUT', 10)) as proxy:
        try:
            proxy.sync()
        except RpcTimeout:
            write_stderr('Odoo sync timeout.\n')


def _supersync(ctx):
    while 1:
        write_stdout('READY\n')
        header_line = sys.stdin.readline()
        headers = dict([x.split(':') for x in header_line.split()])
        data_line = sys.stdin.read(int(headers['len']))
        data = dict([x.split(':') for x in data_line.split()])
        if data['processname'] == 'odoo':
            write_stderr('Odoo here\n')
            _sync_odoo(load_config(ctx.parent.parent.params['config']))
        else:
            write_stderr('Ignoring {}\n'.format(data['processname']))
        write_stdout('RESULT 2\nOK')


@click.command(help='Supervisor eventlistener to sync internal'
                    ' database with Odoo.')
@click.pass_context
def supersync(ctx):
    _supersync(ctx)


@click.command(help='Manually sync internal database with Odoo.')
@click.pass_context
def sync(ctx):
    _sync_odoo(load_config(ctx.parent.parent.params['config']))


odoo.add_command(sync)
odoo.add_command(supersync)

