import json
import eventlet
import logging
from nameko.dependency_providers import Config
from nameko.rpc import rpc, RpcProxy
from nameko.web.handlers import http
import os
import subprocess
import time

logger = logging.getLogger(__name__)


GPIO_OPEN_TYPES = {
    # type_name: [(pin_number, pin_value, sleep_seconds_after_issue)]
    'simple_1': [[25, 0, 1], [25, 1, 0]],
    'pin_1': [[25, 0, 1], [25, 1, 2], [28, 0, 5], [28, 1, 0]]
}


class GatekeeperService:
    name = 'gatekeeper'
    config = Config()
    configurator = RpcProxy('config')

    @rpc
    def ping(self):
        return 'pong'

    def gpio_open(self, sequence):
        gpio_cmd = self.config.get('GPIO_CMD', '/usr/local/bin/gpio')
        for pin, value, sleep in sequence:
            if os.getenv('FAKE_GPIO') != '1':
                subprocess.check_call(
                    [gpio_cmd, 'write', str(pin), str(value)])
                eventlet.sleep(sleep)

    def is_opening(self):
        try:
            last_opened = 1  # float(self.redis.get('last_opened'))
            if last_opened:
                logger.debug('Last opened %.2f seconds ago.',
                             time.time() - last_opened)
                if abs(time.time() - last_opened) <= int(
                        self.configurator.get_string('OPEN_DURATION', '0')):
                    logger.info('Ignoring open request as already opening.')
                    return True
        except TypeError:
            # No last_opened in redis
            pass
        except Exception as e:
            logger.exception('Is opening check error: %s', e)

    @rpc
    def do_open(self, open_type='simple_1'):
        logger.debug('Open type: %s - %s', type(open_type), open_type)
        try:
            # Check if open_type is in pre-defined list
            if type(open_type) is str and open_type in GPIO_OPEN_TYPES.keys():
                open_command = GPIO_OPEN_TYPES[open_type]
            elif type(open_type) is list:
                # Test if list has pairs of 3 values
                try:
                    for a, b, c in open_type:
                        pass
                    open_command = open_type
                except ValueError as e:
                    raise Exception('3-value parse error: %s', e)
            else:
                # Check if open_type is a list of sequences.
                try:
                    open_command = json.loads(open_type)
                except json.decoder.JSONDecodeError:
                    raise Exception('Open type not recognized.')
            if not self.is_opening():
                # Open the barrier
                # self.redis.set('last_opened', time.time())
                self.gpio_open(open_command)
                result = {'open_status': 'success',
                          'open_status_message': 'Opened'}
                logger.debug('Open result: success')
                return result
            else:
                # It is in the process of opening, do not disturb.
                logger.debug('Already opened.')
                result = {'open_status_message': 'Already opening',
                          'open_status': 'success'}
                return result
        except Exception as e:
            logger.exception('Open error:')
            return {'open_status': 'failed', 'open_status_message': str(e)}

    @http('POST', '/open')
    def http_open(self, request, user, password):
        logger.info('HTTP open request')
