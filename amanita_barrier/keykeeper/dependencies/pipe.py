import eventlet
from eventlet.greenio import GreenPipe
import logging
from nameko.extensions import Entrypoint
import os

logger = logging.getLogger(__name__)


class Pipe(Entrypoint):
    pipe = None

    def setup(self):
        if not self.container.config['PIPE_ENABLED']:
            logger.info('PIPE not enabled.')
            return
        self.path = self.container.config['PIPE_PATH']
        if os.path.exists(self.path):
            logger.debug('Removing stale PIPE %s.', self.path)
            os.unlink(self.path)
        os.mkfifo(self.path)
        logger.info('Created PIPE %s.', self.path)

    def start(self):
        self.container.spawn_managed_thread(self.run)

    def stop(self):
        if not self.pipe:
            return
        self.pipe.close()
        logger.debug('PIPE %s closed.', self.path)
        try:
            logger.debug('Removing PIPE %s', self.path)
            os.unlink(self.path)
        except Exception:
            logger.error('Error removing %s', self.path)

    def run(self):
        if not self.container.config.get('PIPE_ENABLED', False):
            logger.info('PIPE disabled.')
            return
        try:
            logger.info('Opening keys PIPE %s', self.path)
            fd = os.open(self.path, os.O_RDONLY | os.O_NONBLOCK)
            self.pipe = GreenPipe(fd, 'r')
            while True:
                line = self.pipe.readline()[:-1]
                if line:
                    logger.info('PIPE received: %s', line)
                    self.container.spawn_worker(self, (line,), {})
                eventlet.sleep(.1)
        except ValueError as e:
            if 'readline of closed file' in str(e):
                return
        except Exception:
            logger.exception('Error')


pipe_read = Pipe.decorator
