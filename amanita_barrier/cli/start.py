import click
from setproctitle import setproctitle
import xmlrpc.client


@click.command('start', help='Starts a service if not started.')
@click.argument('service')
def start(service):
    s = xmlrpc.client.ServerProxy('http://127.0.0.1:9001')
    try:
        s.supervisor.startProcess(service)
        click.echo('Service {} is started.'.format(service))
    except xmlrpc.client.Fault as e:
        if 'ALREADY_STARTED' in str(e):
            click.echo('Service {} is already started.'.format(service))
        elif 'BAD_NAME' in str(e):
            click.echo('Service {} not found.'.format(service))
        else:
            raise
