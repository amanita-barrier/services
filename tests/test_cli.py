from click.testing import CliRunner
from mock import patch
import os
import yaml
import pytest

from amanita_barrier.cli import cli
from amanita_barrier.cli.config import save_config, load_config
from amanita_barrier.cli.run import SERVICES


@pytest.fixture(autouse=True)
def create_env(tmpdir):
    os.chdir(tmpdir.strpath)
    os.environ['AMANITA_BARRIER_INSTALL_PREFIX'] = tmpdir.strpath
    config = {
        'SQLALCHEMY_URL': 'sqlite:///' + tmpdir.strpath + '/app.db',
        'AMANITA_BARRIER_PREFIX': tmpdir.strpath,
    }
    open('config.yml', 'w').write(yaml.dump(config))
    yield


def test_install():
    runner = CliRunner()
    with runner.isolated_filesystem():
        # Test without prefix
        result = runner.invoke(cli, ['install'])
        assert 'Missing option "--prefix"' in result.output
        # Test with prefix
        result = runner.invoke(cli, ['install', '--prefix', os.getcwd()])
        assert not result.exception
        assert os.path.isfile('config.yml')
        # Test overwrite config
        open('config.yml', 'w').write('')
        # Test overwrite config
        result = runner.invoke(cli, ['install', '--prefix', os.getcwd()],
                               input='y')
        assert not result.exception
        config = yaml.load(open('config.yml').read())
        assert 'app.db' in config['SQLALCHEMY_URL']
        assert config['AMANITA_BARRIER_PREFIX'] is not None


def test_config(create_env):
    runner = CliRunner()
    # Test save config
    save_config('config.yml', {'test': '1'})
    # Test set value
    runner.invoke(cli, ['config', 'set', 'test2', '2'])
    result = runner.invoke(cli, ['config', 'show', 'test2'])
    assert result.output.strip('\n') == 'test2: 2'
    # Test load config
    load_config('config.yml')
    # Test value not found
    result3 = runner.invoke(cli, ['config', 'show', 'test4'])
    assert result3.output.strip('\n') == ''


def test_db(create_env):
    runner = CliRunner()
    # Init db
    result = runner.invoke(cli, ['db', 'init'])
    assert not result.exception
    # Test app.db overwrite
    result = runner.invoke(cli, ['db', 'init'], input='y')
    assert 'init' in result.output
    # Test init not rewrite
    result = runner.invoke(cli, ['db', 'init'], input='N')
    assert 'init' not in result.output
    # Test upgrade
    result = runner.invoke(cli, ['db', 'upgrade'])
    assert not result.exception


def test_status():
    runner = CliRunner()
    # Test connection refused
    with patch('xmlrpc.client.ServerProxy') as ServerProxy:
        # Test running
        proxy = ServerProxy.return_value
        info = [{'name': 'odoo', 'statename': 'RUNNING',
                 'description': '(pid 22, uptime 1 day, 1:01:01)'}]
        proxy.supervisor.getAllProcessInfo.return_value = info
        result = runner.invoke(cli, ['status'])
        assert 'Service odoo: RUNNING' in result.output
        # Test supervisor down
        ServerProxy.side_effect = Exception('Connection refused')
        result = runner.invoke(cli, ['status'])
        assert 'Supervisor connection refused' in result.output


def test_run():
    runner = CliRunner()
    for service in SERVICES:
        #with patch('os.execlp'):
        result = runner.invoke(cli, ['run', 's'])
        print (result.output)
        assert not result.exception


