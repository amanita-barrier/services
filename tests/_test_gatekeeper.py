from nameko.testing.services import worker_factory
from amanita_barrier.gate import Gate
import os
import pytest
import time

# Do not try to really open
os.environ['FAKE_GPIO'] = '1'


def test_gate_open_simple1():
    service = worker_factory(Gate)
    res = service.do_open('simple_1')
    assert res == {'status': 'success', 'status_message': 'Opened'}


def test_gate_open_wrong_type():
    service = worker_factory(Gate)
    with pytest.raises(Exception, match='Open type not known'):
        service.do_open('uknown')


def test_already_opening():
    service = worker_factory(Gate)
    # Put in Redis opened just now...
    service.redis.get.side_effect = lambda x: time.time()
    # Put in config 1 second opening delay
    service.db.config_get.side_effect = lambda x, y: 1
    res = service.do_open('simple_1')
    assert res == {'status': 'success', 'status_message': 'Already opening'}
