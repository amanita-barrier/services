import click
import xmlrpc.client


@click.command('restart', help='Restart a service.')
@click.argument('service')
def restart(service):
    s = xmlrpc.client.ServerProxy('http://127.0.0.1:9001')
    try:
        s.supervisor.stopProcess(service)
        s.supervisor.startProcess(service)
        click.echo('Service {} is restarted.'.format(service))
    except xmlrpc.client.Fault as e:
        if 'NOT_RUNNING' in str(e):
            click.echo('Service {} is not running.'.format(service))
        elif 'BAD_NAME' in str(e):
            click.echo('Service {} not found.'.format(service))
        else:
            raise
