import eventlet
import logging
import serial
from nameko.extensions import Entrypoint


logger = logging.getLogger(__name__)


class SerialPortEntrypoint(Entrypoint):
    ser = None

    def __init__(self, port, baud_rate=9600):
        self.port = port
        self.baud_rate = baud_rate

    def setup(self):
        self.ser = serial.Serial(self.port, self.baud_rate, timeout=0)

    def start(self):
        self.container.spawn_managed_thread(self.run)

    def stop(self):
        self.ser.close()

    def get_key_from_data(self, data):
        key_pattern = re.compile('^UNKNOWN \[(.+)\] (.+)$')
        match = key_pattern.search(data)
        if not match:            
            logger.warning('Cannot detect key number from data: {}'.format(
                data))
            return
        else:
            return match.group(2)

    def run(self):
        if self.container.config.get('READ_KEYS') != '1':
            logger.debug('Serial keys read disabled.')
            return
        logger.info('Reading port %s', self.port)
        try:
            buffer = ''
            while True:
                # Read data from the port
                buffer = buffer + self.ser.read(self.ser.inWaiting())
                if not buffer:
                    eventlet.sleep(0.01)
                    continue
                if '\n' in buffer:
                    data = buffer.split('\n')
                    self.container.spawn_worker(self, data, {})
                    # Clear buffer
                    buffer = ''
                    data = ''
                eventlet.sleep(0.01)
        except Exception as e:
            logger.error('Serial port read error: %s', e)
            # Re-run after 2 seconds
            eventlet.sleep(2)
            # Re-open port
            self.ser = serial.Serial(self.port, self.baud_rate, timeout=0)
            self.container.spawn_managed_thread(self.run)


serial_read = SerialPortEntrypoint.decorator
