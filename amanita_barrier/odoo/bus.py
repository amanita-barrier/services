from datetime import datetime
import eventlet
import json
import logging
import random
from amanita_barrier.storage.models import Key

logger = logging.getLogger(__name__)


class BusMessage:
    def __init__(self, service):
        self.service = service
        self.odoo = service.odoo
        self.config = service.config
        self.db = service.db

    def on_bus_AddPins(self, msg):        
        all_data = msg['data']
        for data in all_data:
            logger.info('New PIN %s received', data['number'])
            key = Key(number=data['number'], is_enabled=True, key_type='pin',
                      expire_time=data['expire_time'],
                      external_id=data['id'])
            self.db.session.add(key)
            self.db.session.commit()

    def on_bus_RemovePins(self, msg):
        data = msg['data']
        for i in data:
            pin = self.db.session.query(Key).filter_by(
                external_id=i['id'], number=i['number'],
                key_type='pin').one_or_none()
            if pin:
                logger.info('Removing %s Odoo pin', pin.number)
                self.db.session.delete(pin)
                self.db.session.commit()
            else:
                logger.debug('Pin %s not found', i['number'])

    def on_bus_UpdatePins(self, msg):
        data = msg['data']
        for i in data:
            pin = self.db.session.query(Key).filter_by(
                external_id=i['id'], number=i['number'],
                key_type='pin').one_or_none()
            if pin:
                logger.info('Updating %s Odoo pin', i)
                if i['expire_time']:
                    pin.expire_time = datetime.strptime(
                        i['expire_time'], '%Y-%m-%d %H:%M:%S')
                else:
                    pin.expire_time = None
                self.db.session.add(pin)
                self.db.session.commit()
            else:
                logger.debug('Pin %s not found, creating', i['number'])
                self.on_bus_AddPins(msg)

    def on_bus_Ping(self, msg):
        logger.info('Ping received')
        if msg.get('notify_uid'):
            self.odoo.notify_user(msg['notify_uid'], 'Ping reply')

    """

    def on_bus_UpdateSettings(self, msg):
        logger.info('UpdateSettings received')
        settings = msg.get('Settings')
        logger.debug('UpdateSettings: {}'.format(json.dumps(
            settings, indent=2)))
        for k, v in settings.items():
            self.db.config_set(k, v)

    def on_bus_AgentGetLog(self, msg):
        read_lines = msg.get('NumberOfLogLines', 0)
        if os.path.exists(self.config['LOG_FILE']):
            self.odoo.env['res.config.settings'].rpc_bus_send(
                msg.get('ReplyChannel'), json.dumps({
                    'log': subprocess.check_output(
                        'tail -n {} {}'.format(read_lines,
                                               self.config['LOG_FILE']),
                        shell=True).decode()}))
        else:
            logger.warning('Log file does not exists!')

    def on_bus_AgentKeysLoad(self, msg):
        try:
            self.db.keys_update(msg.get('Keys'))
            logger.info('Loaded %s keys from server.', len(msg.get('Keys')))
        except Exception:
            logger.exception('AgentKeysLoad error:')

    def on_bus_AgentKeysUpdate(self, msg):
        try:
            for k, v in msg.get('Keys').items():
                number, k_type = k.split('|')
                self.db.key_set(
                    number=number, key_type=k_type, is_enabled=v['Enabled'])
            logger.info('Updated %s key(s).', len(msg.get('Keys')))
        except Exception:
            logger.exception('AgentKeysUpdate error:')

    def on_bus_AgentKeysDelete(self, msg):
        try:
            keys_to_del = msg.get('Keys')
            for key in keys_to_del:
                number, key_type = key.split('|')
                self.db.key_delete(number, key_type)
            logger.info('Deleted {} key(s).'.format(len(keys_to_del)))
        except Exception:
            logger.exception('AgentKeysDelete error:')
    """
