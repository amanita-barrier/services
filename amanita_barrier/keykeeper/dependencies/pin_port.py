import binascii
import eventlet
import logging
import serial
from eventlet.green import subprocess
from nameko.extensions import Entrypoint
from amanita_barrier.utils import play_sounds


logger = logging.getLogger(__name__)


class PinPortEntrypoint(Entrypoint):
    ser = None

    def setup(self):
        if not self.container.config.get('PINS_ENABLED', False):
            logger.info('PINs disabled.')
            return
        self.port = self.container.config['PINS_PORT']
        self.baud_rate = int(self.container.config.get('PINS_BAUD_RATE', 9600))
        self.read_interval = float(self.container.config.get(
            'PINS_READ_INTERVAL', 0.1))
        self.reopen_port = self.container.config.get('PINS_REOPEN_PORT')

    def start(self):
        if not self.container.config.get('PINS_ENABLED', False):
            return
        self.container.spawn_managed_thread(self.run)

    def open_port(self):
        while True:
            logger.debug('Re-open port.')
            try:
                if self.ser:
                    self.ser.close()
                self.ser = serial.Serial(
                    self.port, self.baud_rate, timeout=0)
                return
            except Exception as e:
                logger.error('Serial port error: %s', e)
                eventlet.sleep(1)

    def reset_port(self, reopen=False):
        if reopen or self.reopen_port:
            self.open_port()
        if self.ser:
            self.ser.flush()
            self.ser.reset_output_buffer()
            self.ser.reset_input_buffer()

    def run(self):
        if not self.container.config.get('PINS_ENABLED', False):
            return
        logger.info('Reading port %s', self.port)
        self.open_port()
        pin_buffer = []
        while True:
            try:
                data = self.ser.readline()
                if not data:
                    # Empty buffer
                    eventlet.sleep(self.read_interval)
                    continue
                elif subprocess.check_call('gpio read 11', shell=True) != 0:
                    # GSM line is up
                    logger.debug('GSM line is up, ignore key press')
                    self.reset_port()
                    continue
                elif subprocess.check_call('gpio read 8', shell=True) != 0:
                    # SIP line is up
                    logger.debug('SIP line is up, ignore key press')
                    self.reset_port()
                    continue
                key_code = binascii.hexlify(data).decode()
                if key_code == '0c':
                    # Pressed # button, reset pin buffer and initiate a call
                    logger.debug('0c recv.')
                    self.reset_port()
                    pin_buffer = []
                    subprocess.check_call('gpio write 10 1', shell=True)
                    continue
                elif key_code == '0b':
                    # Key * pressed, reset the pin buffer
                    logger.debug('0b recv, clear pin buffer.')
                    self.reset_port()
                    pin_buffer = []
                    play_sounds(self.container.config,
                                self.container.config['PINS_KEY_PRESS_SOUNDS'])
                    continue
                else:
                    # append to the pin_buffer.
                    logger.debug('%s recv.', key_code)
                    pin_buffer.append(key_code)
                # Do we have 4 digits code?
                if len(pin_buffer) == 4:
                    # PIN is formed, process access and reset pin_buffer
                    pin = ''.join([k[1] for k in pin_buffer])
                    logger.info('Read PIN: %s', pin)
                    # Reset the buffer for next PIN code
                    pin_buffer = []
                    self.reset_port()
                    self.container.spawn_worker(self, (pin,), {})
            except Exception:
                logger.exception('Read PIN error:')
                self.reset_port(reopen=True)
                eventlet.sleep(1)


pin_read = PinPortEntrypoint.decorator
