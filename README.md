# Amanita Barrier
## Installation
### System deps
```
apt install python3-pip python3-eventlet ansible
```  
### Package
```
pip3 install amanita-barrier
```
### Working environment
```
amanitabar install --prefix=/srv/barrier
amanitabar db init
```
