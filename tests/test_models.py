import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from amanita_barrier.storage.models import Config, Entry, Key, DeclarativeBase


@pytest.fixture
def session():
    """ Create a test database and session
    """
    engine = create_engine('sqlite:///:memory:')
    DeclarativeBase.metadata.create_all(engine)
    session_cls = sessionmaker(bind=engine)
    return session_cls()


@pytest.fixture
def db():
    """ Create a test database and session
    """
    engine = create_engine('sqlite:///:memory:')
    DeclarativeBase.metadata.create_all(engine)
    session_cls = sessionmaker(bind=engine)

    class Db:
        session = session_cls()
    return Db()


def test_config_create_get(session):
    c1 = Config(param='TEST', value='test')
    session.add(c1)
    session.commit()
    c2 = session.query(Config).filter(Config.param == 'TEST').first()
    assert c2.value == 'test'
