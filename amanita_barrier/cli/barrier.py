import click
from nameko.standalone.rpc import ServiceRpcProxy
from nameko.exceptions import RpcTimeout
from .config import load_config


@click.group(help='Barrier management commands.')
def barrier():
    pass


@click.command('open', help='Send open command to the barrier '
               '(default open_type: simple_1).')
@click.pass_context
@click.argument('open_type', required=False, default='simple_1')
def send_open(ctx, open_type):
    config = load_config(ctx.parent.parent.params['config'])
    with ServiceRpcProxy('gatekeeper', config, timeout=2) as p:
        try:
            output = p.do_open(open_type)
            click.echo(output)
        except RpcTimeout:
            click.secho('Gatekeeper timeout', fg='red')


barrier.add_command(send_open)
