FROM ubuntu

RUN apt update -y  && \
    apt install -y python3 python3-pip supervisor rabbitmq-server vim screen git

COPY  . /root/amanita-barrier/

RUN pip3 install -e /root/amanita-barrier/

ENV AMANITA_BARRIER_PREFIX=/srv/barrier
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]