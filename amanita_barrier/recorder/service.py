from datetime import datetime, timedelta
import json
import logging
from nameko.events import event_handler, EventDispatcher
from nameko.dependency_providers import Config
from nameko.rpc import rpc
from nameko.timer import timer
from nameko_sqlalchemy import Database
from amanita_barrier.storage.models import Entry, Key, DeclarativeBase

logging.basicConfig()
logger = logging.getLogger(__name__)


class RecorderService:
    name = 'recorder'
    db = Database(DeclarativeBase)
    dispatch = EventDispatcher()
    config = Config()

    @rpc
    def ping(self):
        return 'pong'

    @event_handler('keykeeper', 'entry')
    def entry_create(self, data):
        logger.debug('Creating entry: %s', json.dumps(data, indent=2))
        entry = Entry()
        with self.db.get_session() as session:
            key_id = data.pop('key_id', False)
            entry = Entry()
            for k, v in data.items():
                setattr(entry, k, v)
            if key_id:
                entry.key = session.query(Key).get(key_id)
            session.add(entry)
            session.commit()
        logger.debug('Entry %s created', entry.id)
        return entry.id

    @timer(interval=3600 * 24)
    def clean_entries(self):
        keep_days = self.config.get('RECORDER_ENTRY_KEEP_DAYS')
        if not keep_days:
            logger.debug(
                'RECORDER_ENTRY_KEEP_DAYS not set, not cleaning entries.')
            return
        keep_from_date = datetime.utcnow() - timedelta(days=int(keep_days))
        with self.db.get_session() as session:
            q = session.query(Entry).filter(Entry.created_at < keep_from_date)
            if q.count():
                logger.info('Cleaning %s entries.', q.count())
                q.delete()
                session.commit()
            else:
                logger.debug('No outdated entries found.')
