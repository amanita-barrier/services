import eventlet
import json
import logging
from nameko.dependency_providers import Config
from nameko.events import event_handler
from nameko.rpc import rpc
from nameko_sqlalchemy import Database
from nameko.timer import timer
from .dependencies.client import BarrierOdooClient, bus as odoo_bus
from amanita_barrier.storage.models import DeclarativeBase, Key, Entry
from .bus import BusMessage


logger = logging.getLogger(__name__)


class OdooService:
    __version__ = '3.1'
    name = 'odoo'
    odoo = BarrierOdooClient()
    config = Config()
    db = Database(DeclarativeBase)

    @rpc
    def ping(self):
        return 'pong'

    @odoo_bus
    def on_odoo_message(self, channel, msg):
        name = msg['Message']  # Method name
        logger.debug('Bus message {}'.format(name))
        bus_message = BusMessage(self)
        if hasattr(bus_message, 'on_bus_{}'.format(name)):
            try:
                getattr(bus_message, 'on_bus_{}'.format(name))(msg)
            except Exception:
                logger.exception('Bus message error:')
        else:
            logger.info('Bus handler not found for {}'.format(name))

    @event_handler('keykeeper', 'entry')
    def entry_create(self, data):
        logger.debug('Odoo recv new entry.')
        if self.agent_initialized.ready():
            # Odoo is connected but let give Recorder a moment to save entry
            eventlet.sleep(0.1)
        self.send_entry_log(data)

    @rpc
    def sync(self):
        logger.info('Sync called.')
        self.sync_guests()

    def send_entry_log(self, data, db_entry=None):
        if not self.agent_initialized.ready():
            logger.info('Waiting for Odoo connection to send entry.')
            self.agent_initialized.wait()
        logger.debug('Sending entry to Odoo: %s', json.dumps(data, indent=2))
        msg = {
            'BarrierId': self.odoo.BARRIER_ID,
            'PanelId': self.odoo.PANEL_ID,
            'BarrierUid': self.odoo.BARRIER_UID,
            'OpenMethod': 'agent',
        }
        try:
            if data.get('key_type') == 'pin':
                # Adjust for Odoo barrier data
                msg['EventType'] = 'guest_pin'
                # Update Guest for Pin
                if data.get('key_id'):
                        key = self.db.session.query(Key).get(data['key_id'])
                        if key.external_id:
                            msg['GuestId'] = key.external_id
            else:
                msg['EventType'] = data['key_type']
            msg.update({
                'Number': data.get('key_number'),
                'Result': data.get('status'),
                'OpenStatus': data.get('open_status'),
                'OpenStatusMessage': data.get('open_status_message'),
                'EntryUid': data.get('uid'),
                'NoPicture': data.get('no_picture'),
            })
            entry_log_id = self.odoo.env['barrier.entry_log'].create_entry_log(
                msg, context={'tracking_disable': True})
            logger.debug('Odoo entry log %s created.', entry_log_id)
            # Update entry's external_id
            if data.get('uid'):
                with self.db.get_session() as session:
                    if not db_entry:
                        db_entry = session.query(
                            Entry).filter_by(uid=data['uid']).one_or_none()
                    if db_entry:
                        db_entry.external_id = entry_log_id
                        db_entry.external_sync = True
                        session.add(db_entry)
                        session.commit()
                        logger.debug('Entry external_id updated.')

        except Exception as e:
            logger.error('Odoo send entry error: %s', e)

    @rpc
    def sync_guests(self):
        if not self.agent_initialized.ready():
            self.agent_initialized.wait()
        logger.debug('Sync guests.')
        guest_ids = self.odoo.env['barrier.guest'].search(
            [('facility', '=', self.odoo.FACILITY_ID),
             ('state', '=', 'open'), ('pin', '!=', False)])
        guests = self.odoo.execute('barrier.guest', 'read', guest_ids,
                                   ['id', 'pin', 'expire_time'])
        logger.debug('Odoo guests: %s', guests)
        # Delete pins that no longer exist in Odoo
        guests_external_ids = [k['id'] for k in guests]
        pins_del = self.db.session.query(Key).filter_by(key_type='pin').filter(
            ~Key.external_id.in_(guests_external_ids))
        pins_del.delete(synchronize_session='fetch')
        self.db.session.commit()
        # Add new pins
        for guest in guests:
            # Get guest pin from local keys storage
            q_key = self.db.session.query(Key).filter_by(
                external_id=guest['id'])
            if q_key.count() == 0:
                logger.info('PIN %s not found, adding.', guest['pin'])
                key = Key(number=guest['pin'], is_enabled=True, key_type='pin',
                          expire_time=guest['expire_time'],
                          external_id=guest['id'])
                try:
                    self.db.session.add(key)
                    self.db.session.commit()
                except Exception:
                    logger.exception('Add key error:')

            else:
                logger.debug('PIN %s found in local db.', guest['pin'])

    @timer(interval=60)
    def sync_entries(self):
        # Get entires that are not synced yet
        with self.db.get_session() as session:
            entries = session.query(Entry).filter(
                Entry.external_sync.is_(False)).filter(
                Entry.key_id.isnot(None))
            for entry in entries:
                data = {
                    'uid': entry.uid,
                    'key_type': entry.key_type,
                    'key_number': entry.key_number,
                    'status': entry.status,
                    'no_picture': True,
                    'open_status': entry.open_status,
                    'open_status_message': entry.open_status_message,
                }
                # Check if this is a guest pin entry
                if (entry.key and entry.key.external_id and
                        entry.key_type == 'pin'):
                    data['guest_id'] = entry.key.external_id
                self.send_entry_log(data)
            else:
                logger.debug('No entries to sync.')
