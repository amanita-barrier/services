import click
import sys

from .barrier import barrier
from .config import config
from .db import db
from .install import install
from .log import log
from .odoo import odoo
from .status import status
from .stop import stop
from .start import start
from .restart import restart
from .run import MyServices
from .test import test
from amanita_barrier import __version__


@click.group(invoke_without_command=True,
             help='Amanita Barrier management utility.')
@click.pass_context
@click.option('--config', default='config.yml',
              help='Absolute path to configuration file config.yml, '
              'if not specified current directory is searched.')
@click.option('--version', is_flag=True, help='Show version and exit.')
def cli(ctx, config, version):
    if ctx.invoked_subcommand is None:
        if version:
            click.echo(__version__)
            sys.exit(0)
        else:
            click.echo('Please enter your command, see --help.')
            sys.exit(1)

cli.add_command(barrier)
cli.add_command(config)
cli.add_command(db)
cli.add_command(install)
cli.add_command(log)
cli.add_command(odoo)
cli.add_command(restart)
cli.add_command(start)
cli.add_command(stop)
cli.add_command(MyServices(name='run', help='Manually run a service.'))
cli.add_command(status)
cli.add_command(test)
