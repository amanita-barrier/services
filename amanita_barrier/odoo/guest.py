import logging

logger = logging.getLogger(__name__)


class Guest:

    def __init__(self, service):
        self.odoo = service.odoo
        self.service = service

    def is_pin_valid(self, pin):
        guest_order_id = self.odoo.env['barrier.guest'].search([
            ('pin', '=', pin),
            ('state', 'not in', ['closed', 'cancelled'])])
        if guest_order_id:
            logger.debug('Found guest order ID %s', guest_order_id)
            return guest_order_id
        else:
            logger.debug('Guest order for pin %s not found', pin)
            return False

    def process_guest_entry(self, guest_id):
        guest_order = self.odoo.env['barrier.guest'].browse(guest_id)
        guest_order.with_context(tracking_disable=True).process_entry()
        logger.debug('Guest  ID %s entry has been processed', guest_id)
