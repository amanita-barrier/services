import eventlet
from eventlet import Event
import json
import logging
from nameko_odoo import OdooClient, OdooConnection, BusEventHandler


logger = logging.getLogger(__name__)
logging.getLogger('odoorpc.rpc.jsonrpclib').setLevel(level=logging.INFO)


class BarrierOdooConnection(OdooConnection):
    agent_initialized = Event()

    def setup(self):
        super(BarrierOdooConnection, self).setup()
        self.agent_uid = self.container.config['AGENT_UID']

    def poll_bus(self):
        self.register_agent()
        super(BarrierOdooConnection, self).poll_bus()

    def register_agent(self):
        logger.debug('Waiting for Odoo connection to get panel data...')
        self.odoo_connected.wait()
        # Get barrier UID
        callpanel_id = self.odoo.env['barrier.callpanel'].search([
            ('uid', '=', self.agent_uid),
            ('user', '=', self.odoo_user)])
        if not callpanel_id:
            logger.info(
                'Callpanel with UID {} not found!'.format(self.agent_uid))
        else:
            self.odoo.PANEL_ID = callpanel_id[0]
            callpanel = self.odoo.env['barrier.callpanel'].browse(
                self.odoo.PANEL_ID)[0]
            # Populate Odoo connection object with barrier ids.
            self.odoo.AGENT_UID = self.agent_uid
            self.odoo.BARRIER_ID = callpanel.barrier.id
            self.odoo.BARRIER_UID = callpanel.barrier.uid
            self.odoo.FACILITY_ID = callpanel.barrier.facility.id
            logger.info('Panel ID: %s, Barrier UID: %s',
                        self.odoo.BARRIER_ID,
                        self.odoo.BARRIER_UID)
            # Add Agent UID to listening channels
            self.add_channel('amanitabar_panel/{}'.format(
                self.odoo.AGENT_UID))
            # Add Barrier UID to listening channels
            self.add_channel('amanitabar_barrier/{}'.format(
                self.odoo.BARRIER_UID))
            # Add Facility ID to listening channels
            self.add_channel('amanitabar_facility/{}'.format(
                self.odoo.FACILITY_ID))
        # Send 1-st message to be omitted.
        self.odoo.env[
            'res.config.settings'].rpc_bus_send('amanitabar_panel/{}'.format(
                self.agent_uid), json.dumps({'Message': 'Ping'}))
        # Notify those waiting for Agent initialization data
        self.agent_initialized.send()


class BarrierOdooClient(OdooClient):
    connection = BarrierOdooConnection()

    def worker_setup(self, ctx):
        super(BarrierOdooClient, self).worker_setup(ctx)
        ctx.service.agent_initialized = self.connection.agent_initialized


class BarrierBusEventHandler(BusEventHandler):
    connection = BarrierOdooConnection()
    check_entrypoint_channels = False


bus = BarrierBusEventHandler.decorator
