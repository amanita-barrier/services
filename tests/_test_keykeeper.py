from nameko.testing.services import worker_factory, entrypoint_hook
from nameko.testing.utils import get_container
from amanita_barrier.entry import Entry


def test_new_entry(runner_factory, rabbit_config):
    runner = runner_factory(rabbit_config, Entry)
    runner.start()
    container = get_container(runner, Entry)
    with entrypoint_hook(container, 'read_pin') as entrypoint:
        assert entrypoint('1') == 1
