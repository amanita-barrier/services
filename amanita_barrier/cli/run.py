import click
import logging
from setproctitle import setproctitle
import psutil
import subprocess
import sys
from .config import ensure_config

logger = logging.getLogger(__name__)

SERVICES = [
    'config',
    'keykeeper',
    'gatekeeper',
    'odoo',
    'recorder',
]


class MyServices(click.MultiCommand):
    def list_commands(self, ctx):
        return SERVICES

    def get_command(self, ctx, name):
        @click.command(name)
        @click.pass_context
        def cmd(ctx):
            ensure_config(ctx.parent.parent.params['config'])
            proc_name = 'amanita_barrier.{}.service'.format(name)
            for p in psutil.process_iter():
                try:
                    if proc_name in p.cmdline():
                        click.echo(
                            'Service {} is already running'.format(name))
                        return
                except (psutil.AccessDenied, psutil.ZombieProcess):
                    pass
            setproctitle(proc_name)
            config = ctx.parent.parent.params.get('config', 'config.yml')
            try:
                proc = subprocess.Popen([
                    'nameko', 'run', 'amanita_barrier.{}.service'.format(name),
                    '--config', config],
                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                for line in iter(proc.stdout.readline, b''):
                    click.echo(line.decode().strip('\n'))
                exitcode = proc.wait()
                click.secho('Exit code: {}'.format(exitcode))
            except (KeyboardInterrupt, SystemExit):
                click.echo('Exit.')
                sys.exit(0)
            except Exception:
                logger.exception('Service %s error:', name)
                sys.exit(-1)
        return cmd
