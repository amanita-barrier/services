import click
import xmlrpc.client
from amanita_barrier import SERVICES


@click.command('stop', help='Stops a running service.')
@click.argument('service')
def stop(service):
    if service == 'all':
        for service in SERVICES:
            stop_service(service)
    else:
        stop_service(service)


def stop_service(service):
    try:
        s = xmlrpc.client.ServerProxy('http://127.0.0.1:9001')
        s.supervisor.stopProcess(service)
        click.echo('Service {} is stopped.'.format(service))
    except xmlrpc.client.Fault as e:
        if 'NOT_RUNNING' in str(e):
            click.echo('Service {} is not running.'.format(service))
        elif 'BAD_NAME' in str(e):
            click.echo('Service {} not found.'.format(service))
        else:
            raise
