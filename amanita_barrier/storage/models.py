from datetime import datetime
import logging
from sqlalchemy import Column, DateTime, Integer, String, Boolean, ForeignKey
from sqlalchemy import UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


class Base(object):
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow,
                        nullable=False)
    updated_at = Column(DateTime, default=datetime.utcnow,
                        onupdate=datetime.utcnow,
                        nullable=False)


DeclarativeBase = declarative_base(cls=Base)


class Key(DeclarativeBase):
    __tablename__ = 'key'
    __table_args__ = (
        UniqueConstraint('number', 'key_type'),
    )

    number = Column(String, nullable=False)
    key_type = Column(String, nullable=False)
    is_enabled = Column(Boolean)
    expire_time = Column(DateTime, nullable=True)
    external_id = Column(Integer, nullable=True)

    def __init__(self, **kwargs):
        self.number = kwargs.get('number')
        self.key_type = kwargs.get('key_type')
        self.is_enabled = kwargs.get('is_enabled')
        # Convert expire_time from string if required
        if kwargs.get('expire_time'):
            if type(kwargs['expire_time']) is str:
                exp_time_str = kwargs['expire_time'].split('.')[0]
                self.expire_time = datetime.strptime(
                    exp_time_str, '%Y-%m-%d %H:%M:%S')
            else:
                self.expire_time = kwargs.get('expire_time')
        self.external_id = kwargs.get('external_id')


class Config(DeclarativeBase):
    __tablename__ = 'config'

    param = Column(String, nullable=False)
    value = Column(String)

    def __init__(self, param, value):
        self.param = param
        self.value = value


class Entry(DeclarativeBase):
    __tablename__ = 'entry'
    __table_args__ = (
        UniqueConstraint('uid'),
    )

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    uid = Column(String, index=True)
    key_id = Column(Integer, ForeignKey('key.id'))
    key = relationship('Key', backref='entries')
    key_number = Column(String)
    key_type = Column(String)
    status = Column(String)
    cause = Column(String)
    open_status = Column(String)
    open_status_message = Column(String)
    external_sync = Column(Boolean, default=False)
    external_id = Column(Integer)
