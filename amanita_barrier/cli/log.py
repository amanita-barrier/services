import click
import xmlrpc.client


@click.command('log',
               help='Show service logs. If service name is not specified show '
                    'supervisor log.')
@click.option('-n', default=1000, type=int)
@click.argument('service', required=False)
@click.pass_context
def log(ctx, n, service):
    server = xmlrpc.client.ServerProxy('http://127.0.0.1:9001')
    if not service:
        # TODO: Really last lines not like this
        click.echo(server.supervisor.readLog(-n, 0))
    else:
        # Get all process and check if requested process is there
        proc_info = server.supervisor.getAllProcessInfo()
        if not list(filter(lambda x: x['name'] == service, proc_info)):
            click.echo('Service {} not found, is it enabled?'.format(service))
            return
        log = server.supervisor.tailProcessStdoutLog(service, 0, n)[0]
        click.echo(log)
