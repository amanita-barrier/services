import click
import logging
import subprocess
import os
import sys
import yaml
from .config import load_default_config

logger = logging.getLogger(__name__)


def install_playbook(ctx, playbook_path, params=None):
    click.secho('Installing from %s' % playbook_path, bold=True)
    cmd = ['ansible-playbook', '--connection=local',
           '--inventory', '127.0.0.1,', '{}'.format(playbook_path)]
    # Inject vars into every playbook
    config = ctx.parent.params.get('config')
    cmd.extend(['--extra-vars=@{}'.format(config)])
    if params:
        cmd.extend(list(params))
    data = subprocess.Popen(cmd,
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in iter(data.stdout.readline, b''):
        click.echo(line.decode().strip('\n'))
    exitcode = data.wait()
    if exitcode != 0:
        click.secho('Template exit code: {}'.format(exitcode),
                    bold=True, fg='red')
        sys.exit(exitcode)


@click.command(context_settings=dict(ignore_unknown_options=True),
               help='Install required dependencies and '
                    'prepare working environment.')
@click.option('--prefix', help='Target directory for config and db',
              required=True)
@click.option('--config-only', is_flag=True)
@click.argument('params', nargs=-1, type=click.UNPROCESSED)
@click.pass_context
def install(ctx, prefix, config_only, params):
    # Create target directory
    if not os.path.isdir(prefix):
        click.secho('Creating {} directory.'.format(prefix))
        try:
            os.mkdir(prefix)
        except Exception as e:
            click.secho('Cannot create prefix directory: {}'.format(e),
                        bold=True, fg='red')
            sys.exit(-1)
    else:
        click.secho('Notice: directory {} already exists.'.format(prefix))
    # Render default template with prefix
    # Save config to target directory
    config_data = yaml.dump(load_default_config(ctx))
    target_conf = os.path.join(prefix, 'config.yml')
    if os.path.exists(target_conf):
        click.secho('config.yml already exists in {}'.format(prefix))
        if click.confirm('Overwrite?'):
            if click.confirm('Create a backup config.yml.backup?'):
                open(os.path.join(
                    prefix, 'config.yml.backup'), 'w').write(config_data)
                click.echo('config.yml.backup has been created.')
            open(target_conf, 'w').write(config_data)
            click.echo('Existing config.yml is replaced with default one.')
        else:
            click.echo('Keeping existing config.yml.')
    else:
        # Config not yet created
        open(target_conf, 'w').write(config_data)
    # Ok. let's render the playbook.
    if not config_only:
        os.chdir(prefix)
        install_yml = os.path.join(os.path.dirname(__file__), 'install.yml')
        install_playbook(ctx, install_yml, params)
