from datetime import datetime
import eventlet
import json
import logging
from nameko.dependency_providers import Config
from nameko.events import EventDispatcher
from nameko.rpc import RpcProxy, rpc
from nameko_sqlalchemy import Database
import uuid

from amanita_barrier.storage.models import Key, DeclarativeBase
from .dependencies.pin_port import pin_read
from .dependencies.pipe import pipe_read
from amanita_barrier.utils import play_sounds


logger = logging.getLogger(__name__)


class KeykeeperService:
    name = 'keykeeper'
    gatekeeper = RpcProxy('gatekeeper')
    odoo = RpcProxy('odoo')
    config = Config()
    configurator = RpcProxy('config')
    dispatch = EventDispatcher()
    db = Database(DeclarativeBase)

    @rpc
    def ping(self):
        return 'pong'

    @pipe_read
    def pipe_read(self, data):
        logger.debug('PIPE: %s', data)
        number, k_type = data.split('|')
        self.safe_handle_key(number, k_type)

    @pin_read
    def read_pin(self, pin):
        # Check if barrier PIN reading is enabled
        # TODO:
        """
        if self.configurator.get_string('READ_PINS') != '1':
            logger.debug("Barrier's PINs not enabled. Not accepting %s", pin)
            self.dispatch('entry', {
                'status_message': 'pins_not_accepted',
                'status': 'refused',
            })
            return
        """
        try:
            self.safe_handle_key(pin, 'pin')
        except Exception:
            logger.exception('Key handle error:')

    def safe_handle_key(self, *args, **kwargs):
        try:
            return self.handle_key(*args, **kwargs)
        except Exception:
            logger.exception('Handle key error:')

    def handle_key(self, number, key_type):
        key = self.key_get(number, key_type, rpc=False)
        if not key:
            logger.info('Key %s not found', number)
            self.dispatch('entry', {
                'uid': str(uuid.uuid4()),
                'key_type': key_type,
                'key_number': number,
                'status': 'number_not_found',
            })
            if key_type == 'pin':
                play_sounds(self.config,
                            self.config['PINS_BAD_PIN'])
            return
        # Check if the key is enabled
        if not key.is_enabled:
            logger.info('Key %s is disabled.', number)
            self.dispatch('entry', {
                'uid': str(uuid.uuid4()),
                'key_id': key.id,
                'key_type': key_type, 'key_number': number,
                'status': 'key_disabled'
            })
            return
        # Check if the key is expired
        if key.expire_time:
            if datetime.utcnow() > key.expire_time:
                logger.info('Key %s is expired.', number)
                self.dispatch('entry', {
                    'uid': str(uuid.uuid4()),
                    'key_id': key.id,
                    'key_type': key_type, 'key_number': number,
                    'status': 'key_expired'
                })
                return
        logger.info('Key %s is accepted.', number)
        data = {
            'uid': str(uuid.uuid4()),
            'key_id': key.id,
            'key_number': key.number,
            'key_type': key.key_type,
            'status': 'accepted',
        }
        if key_type == 'pin':
            play_sounds(self.config, self.config['PINS_PIN_ACCEPTED_SOUNDS'])
            eventlet.sleep(5)  # Wait 5 seconds to allow to get back in the car
            open_result = self.gatekeeper.do_open(
                self.config.get('PINS_OPEN_COMMAND', 'pin_1'))
            data.update(open_result)
        else:
            data.update(self.gatekeeper.do_open())
        self.dispatch('entry', data)
        logger.debug('Entry dispatch: %s', json.dumps(data, indent=2))

    @rpc
    def key_create(self, number, key_type='key'):
        try:
            k = Key()
            k.number = number
            k.key_type = key_type
            self.db.session.add(k)
            self.db.session.commit()
        except Exception:
            logger.exception('Error')
            raise

    @rpc
    def key_get(self, number, key_type, rpc=True):
        key = self.db.session.query(Key).filter(
            Key.number == number, Key.key_type == key_type).first()
        if rpc:
            return {
                'number': key.number,
                'key_type': key.key_type,
                'is_enabled': key.is_enabled,
                'expire_time': key.expire_time,
                'external_id': key.external_id,
            } if key else {}
        else:
            return key

    @rpc
    def key_delete(self, number, key_type):
        key = self.db.session.query(Key).filter(
            Key.number == number, Key.key_type == key_type).first()
        self.db.session.delete(key)
        self.db.session.commit()

    @rpc
    def key_set(self, number, key_type, is_enabled):
        key = self.db.session.query(Key).filter(
            Key.number == number, Key.key_type == key_type).first()
        if not key:
            logger.warning('Key %s|%s not found, creating', number, key_type)
            key = Key({
                'number': number,
                'key_type': key_type,
                'is_enabled': is_enabled,
            })
            self.db.session.add(key)

        else:
            key.is_enabled = is_enabled
            self.db.session.add(key)
        self.db.session.commit()

    @rpc
    def keys_update(self, keys):
        for k, v in keys.items():
            number, key_type = k.split('|')
            key = self.db.session.query(Key).filter(
                Key.number == number, Key.key_type == key_type).first()
            if key:
                key.is_enabled = v['Enabled']
                self.db.session.add(key)
            else:
                key = Key({
                    'number': number,
                    'key_type': key_type,
                    'is_enabled': v['Enabled']
                })
                self.db.session.add(key)
        self.db.session.commit()
