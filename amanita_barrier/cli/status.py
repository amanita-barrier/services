import click
import xmlrpc.client


@click.command('status', help='Show services status.')
def status():
    try:
        s = xmlrpc.client.ServerProxy('http://127.0.0.1:9001')
        info = s.supervisor.getAllProcessInfo()
        for proc in info:
            click.echo('Service {}: {} ({})'.format(
                proc['name'], proc['statename'], proc['description']))
    except Exception as e:
        if 'Connection refused' in str(e):
            click.secho('Supervisor connection refused', fg='red', bold=True)
        else:
            raise
