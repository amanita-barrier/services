from alembic.config import Config
from alembic import command
import click
import os
import sys
import subprocess
import yaml
from .config import ensure_config, load_config


@click.group(help='Database management.')
@click.pass_context
def db(ctx):
    pass


def _upgrade_db(ctx, init=False):
    config = load_config(ctx.parent.parent.params['config'])
    db_path = os.path.join(config['AMANITA_BARRIER_PREFIX'], 'app.db')
    os.environ['AMANITA_BARRIER_PREFIX'] = config['AMANITA_BARRIER_PREFIX']
    if init and os.path.exists(db_path):
        if click.confirm('app.db already exists. Delete?'):
            os.unlink(db_path)
            click.echo('app.db has been deleted.')
    curdir = os.getcwd()
    try:
        os.chdir(os.path.join(os.path.dirname(__file__), '..'))
        output = subprocess.check_output('alembic upgrade head', shell=True)
        click.echo(output.decode())
    except subprocess.CalledProcessError as e:
        click.secho('Alembic error:\n{}'.format(e.output.decode()))
        sys.exit(-2)
    finally:
        os.chdir(curdir)


@click.command('init',
               help='Initialize database in current working environment.')
@click.pass_context
def db_init(ctx):
    _upgrade_db(ctx, init=True)


@click.command('upgrade', help='Run database migration procedure.')
@click.pass_context
def db_upgrade(ctx):
    _upgrade_db(ctx)


@click.command('revision', help='Generate next revision.')
@click.option('--message', required=True, help='Revision message.')
@click.pass_context
def db_revision(ctx, message):
    # Load config values
    config = load_config(ctx.parent.parent.params['config'])
    sqlalchemy_url = config['SQLALCHEMY_URL']
    script_location = os.path.join(os.path.dirname(__file__), '..', 'alembic')
    # Configure Alembic path
    alembic_cfg = Config()
    alembic_cfg.set_main_option("script_location", script_location)
    alembic_cfg.set_main_option("sqlalchemy.url", sqlalchemy_url)
    command.revision(alembic_cfg, message=message, autogenerate=True)


db.add_command(db_init)
db.add_command(db_upgrade)
db.add_command(db_revision)
