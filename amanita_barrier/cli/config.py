import click
import jinja2
import os
import sys
import yaml


@click.group(help='Configuration management.')
def config():
    pass


def ensure_config(config):
    if not config:
        config = 'config.yml'
    # Search for config.yml in current directory
    if not os.path.exists(config):
        click.secho('{} not found, '
                    'pls set --config option.'.format(config),
                    bold=True, fg='red')
        sys.exit(-1)


def load_default_config(ctx):
    # Get prefix var
    prefix = os.getenv('AMANITA_BARRIER_INSTALL_PREFIX',
                       ctx.params['prefix'])
    default_config_path = os.path.join(
        os.path.dirname(__file__), '..', 'config.default.yml')
    t = jinja2.Template(open(default_config_path).read())
    t_vars = {
        'PREFIX': prefix,
        'SUPERVISORD_CONF_DIR': os.getenv('SUPERVISORD_CONF_DIR',
                                          '/etc/supervisor/'),
        'SUPERVISORD_LOG_DIR': os.getenv('SUPERVISORD_LOG_DIR',
                                         '/var/log/supervisor')
    }
    return yaml.safe_load(t.render(t_vars))


def load_config(config_path):
    ensure_config(config_path)
    config = yaml.safe_load(open(config_path).read())
    return config


def save_config(config_path, data):
    ensure_config(config_path)
    open(config_path, 'w').write(yaml.dump(data))


"""
@click.command('set', help='Set a configuration option.')
@click.argument('option')
@click.pass_context
@click.argument('value')
def set_config(ctx, option, value):
    config_path = ctx.parent.parent.params['config']
    ensure_config(config_path)
    config = load_config(config_path)
    config[option] = value
    save_config(config_path, config)
"""

@click.command('del', help='Delete a configuration option.')
@click.argument('option')
@click.pass_context
def del_config(ctx, option):
    config = load_config(ctx.parent.parent.params['config'])
    if option not in config:
        click.secho('Option {} is not configured.'.format(option))
        return
    del config[option]
    save_config(ctx.parent.parent.params['config'], config)


@click.command('show', help='Show configuration. If name is given, show all'
                            'options mathing this name.')
@click.argument('name', required=False)
@click.pass_context
def show_config(ctx, name=None):
    config = load_config(ctx.parent.parent.params['config'])
    if not name:
        click.echo(yaml.dump(config))
    else:
        # Filter config to options like specified
        for o in config:
            if name in o:
                click.echo('{}: {}'.format(o, config[o]))


@click.command('update',
               help='Update working environment configuration file with '
                    'new paramaters from the default configuration file.')
@click.pass_context
def update_config(ctx):
    config = load_config(ctx.parent.parent.params['config'])
    ctx.params['prefix'] = config['AMANITA_BARRIER_PREFIX']
    default_config = load_default_config(ctx)
    for k in default_config.keys():
        if k not in config.keys():
            config[k] = default_config[k]
            click.echo('Adding {}: {}'.format(k, default_config[k]))
    save_config(ctx.parent.parent.params['config'], config)


# config.add_command(set_config)
config.add_command(del_config)
config.add_command(show_config)
config.add_command(update_config)
