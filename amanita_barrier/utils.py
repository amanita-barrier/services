import logging
import os
from eventlet.green import subprocess

logger = logging.getLogger(__name__)


def play_sounds(config, file_names):
    try:
        # Get config data
        language = config.get('LANGUAGE', 'en')
        sound_cmd = config.get('SOUND_PLAY_CMD', 'aplay -D plughw:0')
        gpio_cmd = config.get('GPIO_CMD', '/usr/local/bin/gpio')
        # Disable keys sounds
        if not os.getenv('FAKE_GPIO'):
            subprocess.check_call([gpio_cmd, 'write', '23', '1'])
            subprocess.check_call([gpio_cmd, 'write', '4', '1'])
        # Play sounds
        for file_name in file_names:
            file_path = os.path.join(
                os.path.dirname(__file__), 'data', 'sounds',
                language, file_name)
            subprocess.check_call(sound_cmd.split(' ') + [file_path])
        # Restore keys sounds
        if not os.getenv('FAKE_GPIO'):
            subprocess.check_call([gpio_cmd, 'write', '23', '0'])
            subprocess.check_call([gpio_cmd, 'write', '4', '0'])
    except Exception:
        logger.exception('Play sound error:')
