import logging
from nameko.rpc import rpc
from nameko_sqlalchemy import Database
from amanita_barrier.storage.models import Config, DeclarativeBase

logger = logging.getLogger(__name__)


class ConfigService:
    """
    This service is responsible for other services configuration.
    """
    name = 'config'
    db = Database(DeclarativeBase)

    @rpc
    def ping(self):
        return 'pong'

    @rpc
    def get_string(self, param, default_value=None):
        """Get a configuration paramater in string format.

        Args:
            param (str): required parameter name.
            default_value (str): (optional) value that is returned if
                                requsted paramter does not exist.

        >>> n.rpc.config.get_string('PINS_ENABLED')
        '1'
        >>> n.rpc.config.get_string('PINS_LEN')
        >>> n.rpc.config.get_string('PINS_LEN', '4')
        '4'
        """
        rec = self.db.session.query(Config).filter(
            Config.param == param).first()
        return rec.value if rec else default_value

    @rpc
    def set_string(self, param, value):
        """Set configuration parameter in string format.

        Args:
            param (str): parameter name that is changed;
            value (str): value to be set.

        >>> n.rpc.config.set_string('PINS_ENABLED', '1')
        True
        """
        # check if the value already exists:
        rec = self.db.session.query(Config).filter(
            Config.param == param).first()
        if not rec:
            rec = Config(param, value)
        else:
            rec.value = value
        self.db.session.add(rec)
        self.db.session.commit()
        return True
