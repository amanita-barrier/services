import click
import nameko
from nameko.standalone.rpc import ServiceRpcProxy
import subprocess
from amanita_barrier import SERVICES
from .config import load_config


@click.command('test',
               help='Test services status.')
@click.option('--timeout', help='Service ping reply timeout.',
              default=3, type=int)
@click.pass_context
def test(ctx, timeout):
    # Test service ping
    for service in SERVICES:
        ping_service(service, load_config(ctx.parent.params['config']),
                     timeout)
    # Test supervisor TODO:


def ping_service(service, config, timeout):
    with ServiceRpcProxy(service, config=config, timeout=timeout) as p:
        try:
            reply = p.ping()
            if reply == 'pong':
                click.echo('[OK] {} replied.'.format(service.capitalize()))
            else:
                click.echo('[FAIL] {} bad reply: {}'.format(
                    service.capitalize(), reply))
        except nameko.exceptions.RpcTimeout:
            click.secho('[FAIL] {} timeout'.format(
                service.capitalize()), fg='red')
        except Exception as e:
            click.secho('[FAIL] {} error: {}'.format(service.capitalize(), e),
                        fg='red')
